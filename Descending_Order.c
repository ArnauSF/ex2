//Libraries
#include <stdio.h>

//Constants
#define MAX_NUMBERS 99
#define MAX_SIZE 10
int main () {
    int number [MAX_NUMBERS]; 
    int i, j, a;

    for (i = 0 ; i < MAX_SIZE ; i++) {
        printf("Enter a number #%d =\n", i+1);
        scanf("%d", &number[i]);
    }
    for (i = 0; i < MAX_SIZE; i++) {
        for (j = i + 1; j < MAX_SIZE; j++) {
            if (number[i] < number[j]) {
                a = number[i];
                number[i] = number[j];
                number[j] = a;
            }
        }
    }
    printf("The numbers arranged in descending order are given: [ %d , ", number[0]);
    for (i = 1; i < MAX_SIZE - 1; ++i) {
        printf("%d , ", number[i]);
    }
    printf("%d ]", number[MAX_SIZE - 1]);

    return 0;
}